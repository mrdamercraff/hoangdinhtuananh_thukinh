export let renderGlass = (list) => {
  let innerHTML = "";
  list.forEach((item) => {
    let { id, src, virtualImg, brand, name, color, price, description } = item;
    innerHTML += `
    <img id='${id}'  onclick="changeClass(this.id)" class='col-4' src="${src}" alt="" />
    `;
  });
  document.getElementById("vglassesList").innerHTML = innerHTML;
};
export let changeClass = (idValue) => {
  let dataGlasses = [
    {
      id: "G1",
      src: "./img/g1.jpg",
      virtualImg: "./img/v1.png",
      brand: "Armani Exchange",
      name: "Bamboo wood",
      color: "Brown",
      price: 150,
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
    },
    {
      id: "G2",
      src: "./img/g2.jpg",
      virtualImg: "./img/v2.png",
      brand: "Arnette",
      name: "American flag",
      color: "American flag",
      price: 150,
      description:
        "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
    },
    {
      id: "G3",
      src: "./img/g3.jpg",
      virtualImg: "./img/v3.png",
      brand: "Burberry",
      name: "Belt of Hippolyte",
      color: "Blue",
      price: 100,
      description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
    },
    {
      id: "G4",
      src: "./img/g4.jpg",
      virtualImg: "./img/v4.png",
      brand: "Coarch",
      name: "Cretan Bull",
      color: "Red",
      price: 100,
      description: "In assumenda earum eaque doloremque, tempore distinctio.",
    },
    {
      id: "G5",
      src: "./img/g5.jpg",
      virtualImg: "./img/v5.png",
      brand: "D&G",
      name: "JOYRIDE",
      color: "Gold",
      price: 180,
      description:
        "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
    },
    {
      id: "G6",
      src: "./img/g6.jpg",
      virtualImg: "./img/v6.png",
      brand: "Polo",
      name: "NATTY ICE",
      color: "Blue, White",
      price: 120,
      description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
    },
    {
      id: "G7",
      src: "./img/g7.jpg",
      virtualImg: "./img/v7.png",
      brand: "Ralph",
      name: "TORTOISE",
      color: "Black, Yellow",
      price: 120,
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
    },
    {
      id: "G8",
      src: "./img/g8.jpg",
      virtualImg: "./img/v8.png",
      brand: "Polo",
      name: "NATTY ICE",
      color: "Red, Black",
      price: 120,
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
    },
    {
      id: "G9",
      src: "./img/g9.jpg",
      virtualImg: "./img/v9.png",
      brand: "Coarch",
      name: "MIDNIGHT VIXEN REMIX",
      color: "Blue, Black",
      price: 120,
      description:
        "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
    },
  ];
  dataGlasses.forEach((item) => {
    let innerHTML1 = "";
    let innerHTML2 = "";
    let { id, src, virtualImg, brand, name, color, price, description } = item;
    if (id == idValue) {
      innerHTML1 = `
     <img id="virtualID" src="${virtualImg}" alt="" />
      `;
      innerHTML2 = `              <div class="d-flex">
      <p class="text-uppercase">${name} -</p>

      <p>${brand} (${color})</p>
    </div>
    <div class="d-flex justify-content-start align-items-center">
      <p class="bg-danger text-white p-1 m-0 rounded">${price}</p>
      <p class="text-success mx-2 my-0">stocking</p>
    </div>
    <div class="mt-4">
      <p>
      ${description}
      </p>
    </div>`;
    } else {
      return;
    }
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("avatar").innerHTML = innerHTML1;
    document.getElementById("glassesInfo").innerHTML = innerHTML2;
  });
};
export let removeGlass = (isNot) => {
  if (isNot) {
    document.getElementById("virtualID").style.display = "block";
  } else {
    document.getElementById("virtualID").style.display = "none";
  }
};
